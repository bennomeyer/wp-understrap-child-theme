<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

    <?php get_sidebar( 'footerfull' ); ?>



    </div>
    <!-- #page -->

    <?php wp_footer(); ?>

    <script>
        jQuery(document).ready(function($) {

            /* show main nav on click */
            $(".main-menu-bars > .fa").click(function() {
                $(this).parent().parent().find(".menu-overlay").toggle();
                $('.menu-overlay-sub').toggle();
                $(this).parent().parent().find(".main-menu-bars").toggleClass("menu-visible");
                $(this).parent().parent().find(".main-menu-bars > i").toggleClass("fa-bars");
                $(this).parent().parent().find(".main-menu-bars > i").toggleClass("fa-times");
            });

            /* hide main nav on click outside main nav */
            $(document).click(function(event) {
                if (!$(event.target).closest(".menu-overlay").length && !$(event.target).closest('.main-menu-bars').length) {
                    if ($(".menu-overlay").is(":visible")) {
                        $(".menu-overlay").css("display", "none");
                        $(".menu-visible .fa-times").addClass("fa-bars").removeClass("fa-times");
                        $(".main-menu-bars").removeClass("menu-visible");
                    }
                }

            });

            /* hide main nav on nav click */
            /* see in the scroll function below */

            /* nav middle gap */
            /*
            function middleGap (nr) {
                return nr/2+1;
            }

            $("#menu-das-angebot li:nth-child(" + middleGap($("#menu-das-angebot li").length) + ")").addClass("push-left");
            $("#menu-der-e-shop li:nth-child(" + middleGap($("#menu-der-e-shop li").length) + ")").addClass("push-left");
            $("#menu-die-kunst li:nth-child(" + middleGap($("#menu-die-kunst li").length) + ")").addClass("push-left");
            $("#menu-der-laden li:nth-child(" + middleGap($("#menu-der-laden li").length) + ")").addClass("push-left");
            */

            /* set content height to window height */
            var windowHeight = $(window).height();
            $(".abschnitt").css("height", windowHeight);


            /* mobile nav - remove links on parent pages */
            $('.menu-mobile-container .menu-item-has-children > a').removeAttr("href");

            /* mobile nav - add hidden */
            $('.menu-mobile-container ul.sub-menu').addClass('hidden');

            /* mobile nav - show sub menu on click */
            $('.menu-mobile-container .menu-item-has-children > a').click(function() {
                $(this).next('.sub-menu').toggleClass('hidden');
            });



            /* detect touch */
            if ("ontouchstart" in window) {
                document.documentElement.className = document.documentElement.className + " touch";
            }
            if (!$("html").hasClass("touch")) {
                /* background fix */
                $(".parallax").css("background-attachment", "fixed");
            }

            /* fix vertical when not overflow
    call fullscreenFix() if .fullscreen content changes */
            function fullscreenFix() {
                var h = $('body').height();
                // set .fullscreen height
                $(".content-b").each(function(i) {
                    if ($(this).innerHeight() > h) {
                        $(this).closest(".fullscreen").addClass("overflow");
                    }
                });
            }
            $(window).resize(fullscreenFix);
            fullscreenFix();

            /* resize background images */
            function backgroundResize() {
                var windowH = $(window).height();
                $(".background").each(function(i) {
                    var path = $(this);
                    // variables
                    var contW = path.width();
                    var contH = path.height();
                    var imgW = path.attr("data-img-width");
                    var imgH = path.attr("data-img-height");
                    var ratio = imgW / imgH;
                    // overflowing difference
                    var diff = parseFloat(path.attr("data-diff"));
                    diff = diff ? diff : 0;
                    // remaining height to have fullscreen image only on parallax
                    var remainingH = 0;
                    if (path.hasClass("parallax") && !$("html").hasClass("touch")) {
                        var maxH = contH > windowH ? contH : windowH;
                        remainingH = windowH - contH;
                    }
                    // set img values depending on cont
                    imgH = contH + remainingH + diff;
                    imgW = imgH * ratio;
                    // fix when too large
                    if (contW > imgW) {
                        imgW = contW;
                        imgH = imgW / ratio;
                    }
                    //
                    path.data("resized-imgW", imgW);
                    path.data("resized-imgH", imgH);
                    path.css("background-size", imgW + "px " + imgH + "px");
                });
            }
            $(window).resize(backgroundResize);
            $(window).focus(backgroundResize);
            backgroundResize();

            /* set parallax background-position */
            function parallaxPosition(e) {
                var heightWindow = $(window).height();
                var topWindow = $(window).scrollTop();
                var bottomWindow = topWindow + heightWindow;
                var currentWindow = (topWindow + bottomWindow) / 2;
                $(".parallax").each(function(i) {
                    var path = $(this);
                    var height = path.height();
                    var top = path.offset().top;
                    var bottom = top + height;
                    // only when in range
                    if (bottomWindow > top && topWindow < bottom) {
                        var imgW = path.data("resized-imgW");
                        var imgH = path.data("resized-imgH");
                        // min when image touch top of window
                        var min = 0;
                        // max when image touch bottom of window
                        var max = -imgH + heightWindow;
                        // overflow changes parallax
                        var overflowH = height < heightWindow ? imgH - height : imgH - heightWindow; // fix height on overflow
                        top = top - overflowH;
                        bottom = bottom + overflowH;
                        // value with linear interpolation
                        var value = min + (max - min) * (currentWindow - top) / (bottom - top);
                        // set background-position
                        var orizontalPosition = path.attr("data-oriz-pos");
                        orizontalPosition = orizontalPosition ? orizontalPosition : "50%";
                        $(this).css("background-position", orizontalPosition + " " + value + "px");
                    }
                });
            }
            if (!$("html").hasClass("touch")) {
                $(window).resize(parallaxPosition);
                //$(window).focus(parallaxPosition);
                $(window).scroll(parallaxPosition);
                parallaxPosition();
            }

            /* Smooth Scrolling of all # Links */

            /* only on desktop */
            if ($(window).width() > 768) {

                // Select all links with hashes
                $('a[href*="#"]')
                    // Remove links that don't actually link to anything
                    .not('[href="#"]')
                    .not('[href="#0"]')
                    .click(function(event) {
                        // On-page links
                        if (
                            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                            location.hostname == this.hostname
                        ) {
                            // Figure out element to scroll to
                            var target = $(this.hash);
                            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                            // Does a scroll target exist?
                            if (target.length) {
                                /* hide menu after click */
                                $(".menu-overlay").css("display", "none");
                                $(".menu-visible .fa-times").addClass("fa-bars").removeClass("fa-times");
                                $(".main-menu-bars").removeClass("menu-visible");

                                // Only prevent default if animation is actually gonna happen
                                event.preventDefault();
                                $('html, body').animate({
                                    scrollTop: target.offset().top
                                }, 1000, function() {
                                    // Callback after animation
                                    // Must change focus!
                                    var $target = $(target);
                                    $target.focus();
                                    if ($target.is(":focus")) { // Checking if the target was focused
                                        return false;
                                    } else {
                                        $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                                        $target.focus(); // Set focus again
                                    };
                                });
                            }
                        }
                    });
            }

            /* mobile menu */
            $("#menu-mobile a, #menu-mobile-1 a, #menu-mobile-2 a, #menu-mobile-3 a, #menu-mobile-4 a").on('click', function(eve) {
                //eve.preventDefault(); 
                $(this).toggleClass("down");
            });


            /* Shop Pages */

            $('.woocommerce-product-gallery').css('opacity', 1);

            /* Sub Page Content Nav */
            /*
            var count = 1;
            var numItems = $('.content-wrapper').length;

            $('.sub-page-content-nav .next').click(function() {
                $('.content-wrapper').not('fade-out').addClass('fade-out');
                count++;
                $('.content-wrapper:nth-child(' + count + ')').removeClass('fade-out');
                if (count == numItems) {
                    $('.sub-page-content-nav .next').addClass('fade-out');
                }
                if (count > 1) {
                    $('.sub-page-content-nav .prev').removeClass('fade-out');
                }
            });

            $('.sub-page-content-nav .prev').click(function() {
                $('.content-wrapper').not('fade-out').addClass('fade-out');
                count--;
                $('.content-wrapper:nth-child(' + count + ')').removeClass('fade-out');
                if (count < numItems) {
                    $('.sub-page-content-nav .next').removeClass('fade-out');
                }
                if (count == 1) {
                    $('.sub-page-content-nav .prev').addClass('fade-out');
                }
            }); */

            /* Sub Page Footer Position Adjuster */
            /* on load */
            $('.entry-footer').css('margin-left', -$('.entry-footer').width() / 2);
            /* on resize */
            $(window).resize(function() {
                $('.entry-footer').css('margin-left', -$('.entry-footer').width() / 2);
            });

        });

    </script>

    </body>

    </html>

<?php
/**
 * Template Name: Custom Full Width Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

session_start();

get_header();
$container = get_theme_mod( 'understrap_container_type' );

    $image = get_field('bg');
        /* sizes: 
        thumbnail: 15x150
        medium: 300x300
        medium-large: 768x
        large: 1024x1024
        full: 1920x1080 */

    if ( empty($image)):
        $page_id = get_queried_object_id();
        $parent_id = wp_get_post_parent_id($page_id);
        /* ids: 
        angebot: 25
        e-shop: 27
        kunst: 29
        laden: 31 */
        $image = $_SESSION['bg-'.$parent_id];
    endif;



    if( !empty($image) ): 
        $imgs = wp_get_attachment_image_src( $image, 'medium_large' )[0];
        $imgm = wp_get_attachment_image_src( $image, 'large' )[0];
        $imgxl = wp_get_attachment_image_src( $image, 'full' )[0];
    endif;
    
    ?>
    
    <style type="text/css">
        .bg-wrapper { 
            background: -webkit-linear-gradient(rgba(255, 255, 255, 0), rgba(17, 0, 2, 0.7)), url('<?php echo $imgs; ?>') no-repeat center center;
            background: linear-gradient(rgba(255, 255, 255, 0), rgba(17, 0, 2, 0.7)), url('<?php echo $imgs; ?>') no-repeat center center;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover; 
            position: fixed;
            width: 100%;
            height: 100%;
        }
        
        @media screen and (min-width: 768px)  { 
            .bg-wrapper { 
                background: -webkit-linear-gradient(rgba(255, 255, 255, 0), rgba(17, 0, 2, 0.7)), url('<?php echo $imgm; ?>') no-repeat center center;  
                background: linear-gradient(rgba(255, 255, 255, 0), rgba(17, 0, 2, 0.7)), url('<?php echo $imgm; ?>') no-repeat center center;  
            } 
        }
        @media screen and (min-width: 1024px) { 
            .bg-wrapper { 
                background: -webkit-background: linear-gradient(rgba(255, 255, 255, 0), rgba(17, 0, 2, 0.7)), url('<?php echo $imgxl; ?>') no-repeat center center;  
                background: background: linear-gradient(rgba(255, 255, 255, 0), rgba(17, 0, 2, 0.7)), url('<?php echo $imgxl; ?>') no-repeat center center;  
; 
            } 
        }
    </style>

<div class="bg-wrapper"></div>
<div class="wrapper" id="full-width-page-wrapper">
    
   

	<div class="container<?php // echo esc_html( $container ); ?>" id="content">

		<div class="row">

			<div class="col-md-12 content-area" id="primary">

				<main class="site-main" id="main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>
                    
                        <!-- Hier beginnt der Inhalt --> 

						<?php get_template_part( 'loop-templates/content', 'custompage' ); ?>
						
						<?php // If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :

							comments_template();

						endif; ?>

					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row end -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
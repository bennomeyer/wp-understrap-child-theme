<?php
/**
 * Template Name: Custom Full Width Front Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

session_start();

/* Image filte vars */
$top = "rgba(255, 255, 255, 0)";
$middle = "rgba(0, 0, 0, 0.2)";
$bottom = "rgba(0, 0, 0, 1)";

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper no-padding" id="full-width-page-wrapper">

    <div class="<?php echo esc_html( $container ); ?>" id="content">

        <div class="row">

            <div class="col-md-12 content-area no-padding" id="primary">

                <main class="site-main" id="main" role="main">


                    <!-- Hier beginnt der Inhalt! -->

                    <!-- HOME -->
                    <?php 
                    $id = 20; // ID der Seite
                    $rand = rand(1,3);
                    $image = get_field('bild_'.$rand, $id);
                    $imgs = wp_get_attachment_image_src( $image, 'medium_large' )[0];
                    $imgm = wp_get_attachment_image_src( $image, 'large' )[0];
                    $imgxl = wp_get_attachment_image_src( $image, 'full' )[0];

                    ?>

                    <div id="home" class="fullscreen background parallax" data-img-width="1600" data-img-height="1064" data-diff="100">
                        <style type="text/css">
                            #home {
                                background: -webkit-linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgs; ?>') no-repeat center center;
                                background: linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgs; ?>') no-repeat center center;
                            }

                            @media screen and (min-width: 769px) {
                                #home {
                                    background: -webkit-linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgm; ?>') no-repeat center center;
                                    background: linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgm; ?>') no-repeat center center;
                                }
                            }

                            @media screen and (min-width: 1025px) {
                                #home {
                                    background: -webkit-linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgxl; ?>') no-repeat center center;
                                    background: linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgxl; ?>') no-repeat center center;
                                }
                            }

                        </style>

                        <div class="content-a abschnitt home">
                            <div class="arrow-wrapper-home">
                                <div class="arrow arrow-1"></div>
                                <div class="main-menu-bars">
                                    <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="menu-overlay menu-visible hidden">
                                    <?php // wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
                                    <div class="menu-hauptmenue-container hidden-sm-down">
                                        <ul id="menu-hauptmenue" class="menu">
                                            <li id="menu-item-2396" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-20 current_page_item menu-item-2396"><a href="https://blumenbinder.ch/"><i class="fa fa-2x fa-home" aria-hidden="true"></i></a>
                                            </li>
                                            <li id="menu-item-69" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69">
                                                <a href="#das-angebot"><?php _e('DAS ANGEBOT', 'wpml_theme'); ?></a>
                                            </li>
                                            <li id="menu-item-1315" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1315">
                                                <a href="#die-galerie"><?php _e('DIE GALERIE', 'wpml_theme'); ?></a>
                                            </li>
                                            <!--<li id="menu-item-68" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-68">
                                                <a href="#der-e-shop"><?php _e('DER E-SHOP', 'wpml_theme'); ?></a>
                                            </li>-->
                                            <li id="menu-item-67" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67">
                                                <a href="#die-kunst"><?php _e('DIE KUNST', 'wpml_theme'); ?></a>
                                            </li>
                                            <li id="menu-item-66" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66">
                                                <a href="#der-laden"><?php _e('DER LADEN', 'wpml_theme'); ?></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- mobile menu template part -->
                                    <?php get_template_part('partials/mobile', 'menu'); ?>
                                </div>
                                <div class="arrow arrow-2-shaft"></div>
                                <div class="arrow arrow-2"></div>
                                <div class="arrow arrow-3-shaft"></div>
                                <div class="arrow arrow-3"></div>
                                <div class="arrow arrow-3-head"></div>
                            </div>
                            
                            <!-- Social Media & Language Switcher -->
                            <div class="sm-icons-wrapper">
                                <!-- Social Media -->
                                <div class="fb"><a target="_blank" href="https://www.facebook.com/blumenbinderzurich/"><img src="https://blumenbinder.ch/wp-content/uploads/2019/06/facebook-48.png"></a></div>
                                <div class="insta"><a target="_blank" href="https://www.instagram.com/blumenbinderzh/"><img src="https://blumenbinder.ch/wp-content/uploads/2019/06/instagram-48.png"></a></div>
                                <!-- Language Switcher -->
                                <div class="language-switcher">
                                    <?php
                                    if ( is_user_logged_in() ) {
                                        custom_language_switcher();
                                    } ?>
                                </div>
                                
                            </div>
                            
                            <div class="button-container">
                            
                                <?php 

                                /* News Button */
                                if ( get_field('news', 86) ):
                                    echo '<a href="/der-laden/news"><div class="button news-button">News</div></a>';
                                endif;

                                /* Offene-Stellen Button */
                                if ( is_user_logged_in() ) {
                                    if ( get_field('offene_stellen', 3809) ):
                                        echo '<a href="/offene-stellen"><div class="button offene-stellen-button">Offene Stellen</div></a>';
                                    endif;
                                }; ?>
                                
                            </div>
                            
                            <div class="content-b abschnitt-title abschnitt-logo">
                                <?php the_custom_logo(); ?>
                            </div>
                        </div>
                    </div>


                    <!-- Das Angebot -->
                    <?php 
                    $id = 25; // ID der Seite
                    $image = get_field('bild_1', $id);
                    $_SESSION['bg-25'] = $image;
                    $imgs = wp_get_attachment_image_src( $image, 'medium_large' )[0];
                    $imgm = wp_get_attachment_image_src( $image, 'large' )[0];;
                    $imgxl = wp_get_attachment_image_src( $image, 'full' )[0];;
                    ?>

                    <div id="angebot" class="fullscreen background parallax" data-img-width="1600" data-img-height="1064" data-diff="100">
                        <style type="text/css">
                            #angebot {
                                background: -webkit-linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgs; ?>') no-repeat center center;
                                background: linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgs; ?>') no-repeat center center;
                            }

                            @media screen and (min-width: 769px) {
                                #angebot {
                                    background: -webkit-linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgm; ?>') no-repeat center center;
                                    background: linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgm; ?>') no-repeat center center;
                                }
                            }

                            @media screen and (min-width: 1025px) {
                                #angebot {
                                    background: -webkit-linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgxl; ?>') no-repeat center center;
                                    background: linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgxl; ?>') no-repeat center center;
                                }
                            }

                        </style>
                        <div id="das-angebot" class="content-a abschnitt das-angebot">
                            <div class="arrow-wrapper-home">
                                <div class="arrow arrow-1"></div>
                                <div class="main-menu-bars">
                                    <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="menu-overlay menu-visible hidden">
                                    <?php // wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
                                    <div class="menu-hauptmenue-container hidden-sm-down">
                                        <ul id="menu-hauptmenue" class="menu">
                                            <li id="menu-item-2396" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-20 current_page_item menu-item-2396"><a href="https://blumenbinder.ch/"><i class="fa fa-2x fa-home" aria-hidden="true"></i></a>
                                            </li>
                                            <li id="menu-item-69" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69">
                                                <a href="#das-angebot"><?php _e('DAS ANGEBOT', 'wpml_theme'); ?></a>
                                            </li>
                                            <li id="menu-item-1315" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1315">
                                                <a href="#die-galerie"><?php _e('DIE GALERIE', 'wpml_theme'); ?></a>
                                            </li>
                                            <!--<li id="menu-item-68" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-68">
                                                <a href="#der-e-shop"><?php _e('DER E-SHOP', 'wpml_theme'); ?></a>
                                            </li>-->
                                            <li id="menu-item-67" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67">
                                                <a href="#die-kunst"><?php _e('DIE KUNST', 'wpml_theme'); ?></a>
                                            </li>
                                            <li id="menu-item-66" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66">
                                                <a href="#der-laden"><?php _e('DER LADEN', 'wpml_theme'); ?></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- mobile menu template part -->
                                    <?php get_template_part('partials/mobile', 'menu'); ?>
                                </div>
                                <div class="arrow arrow-2-shaft"></div>
                                <div class="arrow arrow-2"></div>
                                <div class="arrow arrow-3-shaft"></div>
                                <div class="arrow arrow-3"></div>
                                <div class="arrow arrow-3-head"></div>
                            </div>
                            <div class="content-b abschnitt-title">
                                <h2 class="entry-title"><?php _e('DAS', 'wpml_theme'); ?><br><?php _e('ANGEBOT', 'wpml_theme'); ?></h2>
                                <div class="abschnitt-menu hidden-sm-down">
                                    <?php wp_nav_menu( array( 'theme_location' => 'das-angebot' ) ); ?>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- Der E-Shop -->
                    <?php
                    $id = 27; // ID der Seite
                    $image = get_field('bild_1', $id);
                    $_SESSION['bg-27'] = $image;
                    $imgs = wp_get_attachment_image_src( $image, 'medium_large' )[0];
                    $imgm = wp_get_attachment_image_src( $image, 'large' )[0];;
                    $imgxl = wp_get_attachment_image_src( $image, 'full' )[0];;
                    ?>

                    <!-- eshop wird noch nicht angezeigt *** PHP ist auskommentiert! Gradients anpassen!
                    <div id="e-shop" class="not-fullscreen background parallax" data-img-width="1600" data-img-height="1064" data-diff="100">
                        <style type="text/css">
                                #e-shop { 
                                    background: -webkit-linear-gradient(rgba(255, 255, 255, 0), rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 1)), url('<?php // echo $imgs; ?>') no-repeat center center; 
                                    background: linear-gradient(rgba(255, 255, 255, 0), rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 1)), url('<?php // echo $imgs; ?>') no-repeat center center;  
                            }
                            @media screen and (min-width: 769px) { 
                                #e-shop { 
                                    background: -webkit-linear-gradient(rgba(255, 255, 255, 0), rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 1)), url('<?php // echo $imgm; ?>') no-repeat center center; 
                                    background: linear-gradient(rgba(255, 255, 255, 0), rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 1)), url('<?php // echo $imgm; ?>') no-repeat center center;  
                                } 
                            }
                            @media screen and (min-width: 1025px) {
                                #e-shop { 
                                    background: -webkit-linear-gradient(rgba(255, 255, 255, 0), rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 1)), url('<?php // echo $imgxl; ?>') no-repeat center center; 
                                    background: linear-gradient(rgba(255, 255, 255, 0), rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 1)), url('<?php // echo $imgxl; ?>') no-repeat center center; 
                                } 
                            }
                        </style>
                        <div id="der-e-shop" class="content-a abschnitt der-e-shop">
                            <div class="arrow-wrapper-home">
                                <div class="arrow arrow-1"></div>
                                <div class="main-menu-bars">
                                    <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="menu-overlay menu-visible hidden">
                                    <?php // wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
                                    <div class="menu-hauptmenue-container hidden-sm-down">
                                        <ul id="menu-hauptmenue" class="menu">
                                            <li id="menu-item-69" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69">
                                                <a href="#das-angebot"><?php //_e('DAS ANGEBOT', 'wpml_theme'); ?></a>
                                            </li>
                                            <li id="menu-item-68" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-68">
                                                <a href="#der-e-shop"><?php //_e('DER E-SHOP', 'wpml_theme'); ?></a>
                                            </li>
                                            <li id="menu-item-67" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67">
                                                <a href="#die-kunst"><?php //_e('DIE KUNST', 'wpml_theme'); ?></a>
                                            </li>
                                            <li id="menu-item-66" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66">
                                                <a href="#der-laden"><?php //_e('DER LADEN', 'wpml_theme'); ?></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- mobile menu template part -->
                    <?php // get_template_part('partials/mobile', 'menu'); ?>
                    <!--
                                </div>
                                <div class="arrow arrow-2-shaft"></div>
                                <div class="arrow arrow-2"></div>
                                <div class="arrow arrow-3-shaft"></div>
                                <div class="arrow arrow-3"></div>
                                <div class="arrow arrow-3-head"></div>
                            </div>
                            <div class="content-b abschnitt-title">
                                <h2 class="entry-title"><?php //_e('DER', 'wpml_theme'); ?><br><?php //_e('E-SHOP', 'wpml_theme'); ?></h2>
                                <div class="abschnitt-menu hidden-sm-down">
                                    <?php // wp_nav_menu( array( 'theme_location' => 'der-e-shop' ) ); ?>
                                </div>
                            </div>
                        </div>
                    </div> -->

                    <!-- Die Galerie -->
                    <?php
                    $id = 1308; // ID der Seite
                    $image = get_field('bild_1', $id);
                    $_SESSION['bg-1308'] = $image; //Site ID for children
                    $imgs = wp_get_attachment_image_src( $image, 'medium_large' )[0];
                    $imgm = wp_get_attachment_image_src( $image, 'large' )[0];;
                    $imgxl = wp_get_attachment_image_src( $image, 'full' )[0];;
                    ?>

                    <div id="galerie" class="not-fullscreen background parallax" data-img-width="1600" data-img-height="1064" data-diff="100">
                        <style type="text/css">
                            #galerie {
                                background: -webkit-linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgs; ?>') no-repeat center center;
                                background: linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgs; ?>') no-repeat center center;
                            }

                            @media screen and (min-width: 769px) {
                                #galerie {
                                    background: -webkit-linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgm; ?>') no-repeat center center;
                                    background: linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgm; ?>') no-repeat center center;
                                }
                            }

                            @media screen and (min-width: 1025px) {
                                #galerie {
                                    background: -webkit-linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgxl; ?>') no-repeat center center;
                                    background: linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgxl; ?>') no-repeat center center;
                                }
                            }

                        </style>
                        <div id="die-galerie" class="content-a abschnitt die-kunst">
                            <div class="arrow-wrapper-home">
                                <div class="arrow arrow-1"></div>
                                <div class="main-menu-bars">
                                    <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="menu-overlay menu-visible hidden">
                                    <?php // wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
                                    <div class="menu-hauptmenue-container hidden-sm-down">
                                        <ul id="menu-hauptmenue" class="menu">
                                            <li id="menu-item-2396" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-20 current_page_item menu-item-2396"><a href="https://blumenbinder.ch/"><i class="fa fa-2x fa-home" aria-hidden="true"></i></a>
                                            </li>
                                            <li id="menu-item-69" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69">
                                                <a href="#das-angebot"><?php _e('DAS ANGEBOT', 'wpml_theme'); ?></a>
                                            </li>
                                            <li id="menu-item-1315" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1315">
                                                <a href="#die-galerie"><?php _e('DIE GALERIE', 'wpml_theme'); ?></a>
                                            </li>
                                            <!--<li id="menu-item-68" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-68">
                                                <a href="#der-e-shop"><?php _e('DER E-SHOP', 'wpml_theme'); ?></a>
                                            </li>-->
                                            <li id="menu-item-67" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67">
                                                <a href="#die-kunst"><?php _e('DIE KUNST', 'wpml_theme'); ?></a>
                                            </li>
                                            <li id="menu-item-66" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66">
                                                <a href="#der-laden"><?php _e('DER LADEN', 'wpml_theme'); ?></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- mobile menu template part -->
                                    <?php get_template_part('partials/mobile', 'menu'); ?>
                                </div>
                                <div class="arrow arrow-2-shaft"></div>
                                <div class="arrow arrow-2"></div>
                                <div class="arrow arrow-3-shaft"></div>
                                <div class="arrow arrow-3"></div>
                                <div class="arrow arrow-3-head"></div>
                            </div>
                            <div class="content-b abschnitt-title">
                                <h2 class="entry-title"><?php _e('DIE', 'wpml_theme'); ?><br><?php _e('GALERIE', 'wpml_theme'); ?></h2>
                                <div class="abschnitt-menu hidden-sm-down">
                                    <?php wp_nav_menu( array( 'theme_location' => 'die-galerie' ) ); ?>
                                </div>
                            </div>
                        </div>
                    </div>




                    <!-- Die Kunst -->
                    <?php
                    $id = 29; // ID der Seite
                    $image = get_field('bild_1', $id);
                    $_SESSION['bg-29'] = $image;
                    $imgs = wp_get_attachment_image_src( $image, 'medium_large' )[0];
                    $imgm = wp_get_attachment_image_src( $image, 'large' )[0];;
                    $imgxl = wp_get_attachment_image_src( $image, 'full' )[0];;
                    ?>

                    <div id="kunst" class="not-fullscreen background parallax" data-img-width="1600" data-img-height="1064" data-diff="100">
                        <style type="text/css">
                            #kunst {
                                background: -webkit-linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgs; ?>') no-repeat center center;
                                background: linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgs; ?>') no-repeat center center;
                            }

                            @media screen and (min-width: 769px) {
                                #kunst {
                                    background: -webkit-linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgm; ?>') no-repeat center center;
                                    background: linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgm; ?>') no-repeat center center;
                                }
                            }

                            @media screen and (min-width: 1025px) {
                                #kunst {
                                    background: -webkit-linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgxl; ?>') no-repeat center center;
                                    background: linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgxl; ?>') no-repeat center center;
                                }
                            }

                        </style>
                        <div id="die-kunst" class="content-a abschnitt die-kunst">
                            <div class="arrow-wrapper-home">
                                <div class="arrow arrow-1"></div>
                                <div class="main-menu-bars">
                                    <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="menu-overlay menu-visible hidden">
                                    <?php // wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
                                    <div class="menu-hauptmenue-container hidden-sm-down">
                                        <ul id="menu-hauptmenue" class="menu">
                                            <li id="menu-item-2396" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-20 current_page_item menu-item-2396"><a href="https://blumenbinder.ch/"><i class="fa fa-2x fa-home" aria-hidden="true"></i></a>
                                            </li>
                                            <li id="menu-item-69" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69">
                                                <a href="#das-angebot"><?php _e('DAS ANGEBOT', 'wpml_theme'); ?></a>
                                            </li>
                                            <li id="menu-item-1315" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1315">
                                                <a href="#die-galerie"><?php _e('DIE GALERIE', 'wpml_theme'); ?></a>
                                            </li>
                                            <!--<li id="menu-item-68" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-68">
                                                <a href="#der-e-shop"><?php _e('DER E-SHOP', 'wpml_theme'); ?></a>
                                            </li>-->
                                            <li id="menu-item-67" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67">
                                                <a href="#die-kunst"><?php _e('DIE KUNST', 'wpml_theme'); ?></a>
                                            </li>
                                            <li id="menu-item-66" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66">
                                                <a href="#der-laden"><?php _e('DER LADEN', 'wpml_theme'); ?></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- mobile menu template part -->
                                    <?php get_template_part('partials/mobile', 'menu'); ?>
                                </div>
                                <div class="arrow arrow-2-shaft"></div>
                                <div class="arrow arrow-2"></div>
                                <div class="arrow arrow-3-shaft"></div>
                                <div class="arrow arrow-3"></div>
                                <div class="arrow arrow-3-head"></div>
                            </div>
                            <div class="content-b abschnitt-title">
                                <h2 class="entry-title"><?php _e('DIEKUNST', 'wpml_theme'); ?><br><?php _e('KUNST', 'wpml_theme'); ?></h2>
                                <div class="abschnitt-menu hidden-sm-down">
                                    <?php wp_nav_menu( array( 'theme_location' => 'die-kunst' ) ); ?>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- Der Laden -->
                    <?php
                    $id = 31; // ID der Seite
                    $image = get_field('bild_1', $id);
                    $_SESSION['bg-31'] = $image;
                    $imgs = wp_get_attachment_image_src( $image, 'medium_large' )[0];
                    $imgm = wp_get_attachment_image_src( $image, 'large' )[0];;
                    $imgxl = wp_get_attachment_image_src( $image, 'full' )[0];;
                    ?>

                    <div id="laden" class="not-fullscreen background parallax" data-img-width="1600" data-img-height="1064" data-diff="100">
                        <style type="text/css">
                            #laden {
                                background: -webkit-linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgs; ?>') no-repeat center center;
                                background: linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgs; ?>') no-repeat center center;
                            }

                            @media screen and (min-width: 769px) {
                                #laden {
                                    background: -webkit-linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgm; ?>') no-repeat center center;
                                    background: linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgm; ?>') no-repeat center center;
                                }
                            }

                            @media screen and (min-width: 1025px) {
                                #laden {
                                    background: -webkit-linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgxl; ?>') no-repeat center center;
                                    background: linear-gradient(<? echo $top; ?>, <? echo $middle; ?>, <? echo $bottom; ?>), url('<?php echo $imgxl; ?>') no-repeat center center;
                                }
                            }

                        </style>
                        <div id="der-laden" class="content-a abschnitt der-laden">

                            <div class="arrow-wrapper-home">
                                <div class="arrow arrow-1"></div>
                                <div class="main-menu-bars">
                                    <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
                                </div>
                                <div class="menu-overlay menu-visible hidden">
                                    <?php // wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
                                    <div class="menu-hauptmenue-container hidden-sm-down">
                                        <ul id="menu-hauptmenue" class="menu">
                                            <li id="menu-item-2396" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-20 current_page_item menu-item-2396"><a href="https://blumenbinder.ch/"><i class="fa fa-2x fa-home" aria-hidden="true"></i></a>
                                            </li>
                                            <li id="menu-item-69" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69">
                                                <a href="#das-angebot"><?php _e('DAS ANGEBOT', 'wpml_theme'); ?></a>
                                            </li>
                                            <li id="menu-item-1315" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1315">
                                                <a href="#die-galerie"><?php _e('DIE GALERIE', 'wpml_theme'); ?></a>
                                            </li>
                                            <!--<li id="menu-item-68" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-68">
                                                <a href="#der-e-shop"><?php _e('DER E-SHOP', 'wpml_theme'); ?></a>
                                            </li>-->
                                            <li id="menu-item-67" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67">
                                                <a href="#die-kunst"><?php _e('DIE KUNST', 'wpml_theme'); ?></a>
                                            </li>
                                            <li id="menu-item-66" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66">
                                                <a href="#der-laden"><?php _e('DER LADEN', 'wpml_theme'); ?></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- mobile menu template part -->
                                    <?php get_template_part('partials/mobile', 'menu'); ?>
                                </div>
                                <div class="arrow arrow-2-shaft"></div>
                                <div class="arrow arrow-2"></div>
                                <div class="arrow arrow-3-shaft"></div>
                                <div class="arrow arrow-3"></div>
                                <div class="arrow arrow-3-head"></div>
                            </div>
                            <div class="content-b abschnitt-title">
                                <h2 class="entry-title"><?php _e('DER', 'wpml_theme'); ?><br><?php _e('LADEN', 'wpml_theme'); ?></h2>
                                <div class="abschnitt-menu hidden-sm-down">
                                    <?php wp_nav_menu( array( 'theme_location' => 'der-laden' ) ); ?>
                                </div>
                            </div>
                        </div>
                    </div>






                </main><!-- #main -->

            </div><!-- #primary -->

        </div><!-- .row end -->

    </div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>

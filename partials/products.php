<ul class="products">
    <?php
    $args = array(
        'post_type' => 'product',
        'product_cat' => 'straeusse', //set slug of respective products
        'posts_per_page' => 12
    );
    $loop = new WP_Query( $args );

    

    if ( $loop->have_posts() ) {
        while ( $loop->have_posts() ) : $loop->the_post();


        wc_get_template_part( 'content', 'single-product' ); 


        endwhile;
    
    
    } else {
    echo __( 'No products found' );
    }
    wp_reset_postdata();
    ?>
</ul><!--/.products-->

<?php get_footer( 'shop' );

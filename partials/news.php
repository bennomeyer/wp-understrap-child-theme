<?php

global $post;
$args = array(  );

$myposts = get_posts( $args );
$i = 0;
foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
<div class="content-wrapper row">
    <div class="entry-content col-md-6">
        <?php 
        if ( !empty( get_the_title() ) ) {
            the_title( '<h1>', '</h1>' );
        }
        if ( !empty( get_the_content() ) ) {
            the_content();
        } ?>
    </div>
    <div class="entry-content fit-img <?php if ($i == 0) { echo 'fit-img-1st-child'; } ?> col-md-6">
        <?php 
        if ( get_the_post_thumbnail() ) {
            echo get_the_post_thumbnail();
        } ?>
    </div>
</div>
<?php $i++; ?>
<?php endforeach; ?>
<div class="arrow-down hidden-sm-down"></div>
<?php wp_reset_postdata(); ?>
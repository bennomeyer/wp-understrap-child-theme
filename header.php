<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
    <!DOCTYPE html>
    <html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <div id="page-loader">
            <div id="loader"></div>
        </div>
        <style>
            #page-loader {
                position: fixed;
                width: 100%;
                height: 100%;
                background-color: black;
                z-index: 9999999;
            }

            #loader {
                position: fixed;
                left: calc(50% - 60px);
                top: calc(50% - 60px);
                border: 1px solid #f3f3f3;
                border-radius: 50%;
                border-top: 1px solid #666666;
                width: 120px;
                height: 120px;
                -webkit-animation: spin 2s linear infinite;
                /* Safari */
                animation: spin 2s linear infinite;
            }

        </style>
        <script>
            /* Loading Screen Fade Out */
            jQuery(document).ready(function($) {
                $(window).load(function() {
                    $('#page-loader').fadeOut('slow');
                });
            });

        </script>

        <div class="hfeed site" id="page">

            <!-- ******************* The Navbar Area ******************* -->

            <?php if ( ! is_front_page() && ! is_page_template('page-templates/customfullwidthpage.php') ) : ?>

            < div class="wrapper-fluid wrapper-navbar" id="wrapper-navbar">

                < a class="skip-link screen-reader-text sr-only" href="#content">
                    <?php _e( 'Skip to content',
		'understrap' ); ?>
                    < /a>

                        < nav class="navbar navbar-toggleable-md navbar-inverse bg-primary">

                            <?php if ( 'container' == $container ) : ?>
                            < div class="container">
                                <?php endif; ?>

                                < button class="navbar-toggler" type="button" data - toggle="collapse" data - target="#navbarNavDropdown" aria - controls="navbarNavDropdown" aria - expanded="false" aria - label="Toggle navigation">
                                    < span class="navbar-toggler-icon">
                                        < /span>
                                            < /button>

                                                <!-- Your site title as branding in the menu -->
                                                <?php if ( ! has_custom_logo() ) { ?>

                                                <?php if ( is_front_page() && is_home() ) : ?>

                                                < h1 class="navbar-brand mb-0">
                                                    < a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                                                        <?php bloginfo( 'name' ); ?>
                                                        < /a>
                                                            < /h1>

                                                                <?php elseif ( is_page_template('page-templates/customfullwidthpage.php') ) : ?>


                                                                <?php else : ?>

                                                                < a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                                                                    <?php bloginfo( 'name' ); ?>
                                                                    < /a>

                                                                        <?php endif; ?>


                                                                        <?php } else {
    
    				    if ( is_page_template('page-templates/customfullwidthpage.php') ) : 
                        else : 
						  the_custom_logo();
                        endif; 
                            
					} ?>
                                                                        <!-- end custom logo -->

                                                                        <!-- The WordPress Menu goes here -->
                                                                        <?php 
                
                /* get parent page for right sub menu */
                
                if ( is_page_template('page-templates/customfullwidthpage.php') ) : 
                else : 
                    $page_id = get_queried_object_id();
                    $parent_id = wp_get_post_parent_id($page_id);
                    $parent_title = get_the_title($parent_id);
                    $parent_title = strtolower($parent_title);
                    $parent_title = str_replace(" ", "-", $parent_title);

                    wp_nav_menu(
                        array(
                            'menu'            => $parent_title,
                            'theme_location'  => 'primary',
                            'container_class' => 'collapse navbar-collapse',
                            'container_id'    => 'navbarNavDropdown',
                            'menu_class'      => 'navbar-nav sub-nav',
                            'fallback_cb'     => '',
                            'menu_id'         => 'main-menu'
                            // 'walker'          => new WP_Bootstrap_Navwalker(),
                        )
				    ); 
                endif;
                ?>

                                                                        <?php if ( is_active_sidebar( 'navbar-right' ) ) : ?>
                                                                        < div class="navbar-right-widget">
                                                                            <?php dynamic_sidebar( 'navbar-right' ); ?>
                                                                            < /div>
                                                                                <?php endif; ?>
                                                                                <?php if ( 'container' == $container ) : ?>
                                                                                < /div>
                                                                                    <!-- .container -->
                                                                                    <?php endif; ?>

                                                                                    < /nav>
                                                                                        <!-- .site-navigation -->

                                                                                        < /div>
                                                                                            <!-- .wrapper-navbar end -->

                                                                                            <?php endif; ?>

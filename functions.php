<?php
function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();

    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );    
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
}


function first_paragraph($content){
    return preg_replace('/<p([^>]+)?>/', '<p$1 class="intro-content">', $content, 1);
}
add_filter('the_content', 'first_paragraph');

/******************************************************************************************************
 * Filter the except length to 200 characters.
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
******************************************************************************************************/
function wpdocs_custom_excerpt_length( $length ) {
    return 100;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );



/*********
* Adding extra widgets on top of the understrap defaults
************/
if ( ! function_exists( 'draft_widgets_init' ) ) {
    /**
     * Initializes themes widgets.
     */
    function draft_widgets_init() {
        register_sidebar( array(
            'name'          => __( 'Navbar right', 'understrap' ),
            'id'            => 'navbar-right',
            'description'   => 'Widget area in the top right navbar corner',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        ) );

    }
} // endif function_exists( 'understrap_widgets_init' ).
add_action( 'widgets_init', 'draft_widgets_init' );


/* Loading style.css for manual changes */

function my_theme_enqueue_styles() {

    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );


/* Disable WordPress Admin Bar for all users but admins. */
  show_admin_bar(false);


/* Custom Menus */
function register_my_menus() {
  register_nav_menus(
    array(
      'das-angebot' => __( 'Das Angebot' ),
      'die-galerie' => __( 'Die Galerie' ),
      'der-laden' => __( 'Der Laden' ),
      'die-kunst' => __( 'Die Kunst' ),
      'der-e-shop' => __( 'Der E-Shop'),
    )
  );
}
add_action( 'init', 'register_my_menus' );

/* Custom Language Switcher */

function custom_language_switcher() {
    $languages = apply_filters( 'wpml_active_languages', NULL, array( 'skip_missing' => 0 ) );
 
    if ( !empty( $languages ) ) {
        $i = 0;
        $len = count($languages);
        foreach ( $languages as $language ){
            $native_name = $language['active'] ? strtoupper( $language['native_name'] ) : $language['native_name'];
            $language_code = ucfirst( $language['language_code'] ) ; 
        
 
            if ( !$language['active'] ) echo '<a href="' . $language['url'] . '">';
            echo '<span class="lang">' . $language_code . '</span> ';
            if (!$i == $len - 1) echo '| ';
            if ( !$language['active'] ) echo '</a>';
            $i++;
        }
    }
}


/* test lydia angepasste Bildkomprimierung */

add_filter(‚jpeg_quality‘, function($arg) {return 100;});

add_filter(‚wp_editor_set_quality‘, function($arg) {return 100;});




/**
 * Hide email from Spam Bots using a shortcode.
 *
 * @param array  $atts    Shortcode attributes. Not used.
 * @param string $content The shortcode content. Should be an email address.
 *
 * @return string The obfuscated email address. 
 */


function wpcodex_hide_email_shortcode( $atts , $content = null ) {
	if ( ! is_email( $content ) ) {
		return;
	}

	$content = antispambot( $content );

	$email_link = sprintf( 'mailto:%s', $content );

	return sprintf( '<a href="%s">%s</a>', esc_url( $email_link, array( 'mailto' ) ), esc_html( $content ) );
}
add_shortcode( 'email', 'wpcodex_hide_email_shortcode' );


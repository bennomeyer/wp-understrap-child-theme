<?php
/**
 * Partial template for content in page.php
 *
 * @package understrap
 */

?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">
        <div class="arrow-wrapper-sub">
            <div class="arrow arrow-1"></div>
            <div class="arrow arrow-2-shaft"></div>
            <div class="arrow arrow-2"></div>
            <div class="arrow arrow-2-head"></div>
        </div>
        <div class="entry-content col-md-5"><?php the_content(); ?></div>
            <div class="menu-overlay-wrapper">
                <div class="main-menu-bars">
                    <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
                </div>
            </div>
        <div class="menu-overlay menu-visible hidden">
            <?php // wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
            <div class="menu-hauptmenue-container">
                <ul id="menu-hauptmenue" class="menu">
                    <li id="menu-item-69" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>#das-angebot">Das Angebot</a>
                    </li>
                    <li id="menu-item-68" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-68">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>#der-e-shop">Der E-Shop</a>
                    </li>
                    <li id="menu-item-67" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>#die-kunst">Die Kunst</a>
                    </li>
                    <li id="menu-item-66" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>#der-laden">Der Laden</a>
                    </li>
                </ul>
            </div>
        </div>
        
		<?php // the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>

	<div class="entry-content-wrapper">
        
        <div class="horizontal-scroll-wrapper rectangles">
            <div class="rectangle bild-1"><?php 
                $image = get_field('bild_1');
                if ( !empty($image) ): ?>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <?php endif; ?>
            </div>
            <div class="rectangle bild-2"><?php 
                $image = get_field('bild_2');
                if ( !empty($image) ): ?>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <?php endif; ?>
            </div>
            <div class="rectangle bild-3"><?php 
                $image = get_field('bild_3');
                if ( !empty($image) ): ?>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <?php endif; ?>
            </div>            
            <div class="rectangle bild-4"><?php 
                $image = get_field('bild_4');
                if ( !empty($image) ): ?>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <?php endif; ?>
            </div>
            <div class="rectangle bild-5"><?php 
                $image = get_field('bild_5');
                if ( !empty($image) ): ?>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <?php endif; ?>
            </div>            
        
        </div>
        

        

		<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
			'after'  => '</div>',
		) );
		?>

	</div><!-- .entry-content -->

	<footer class="entry-footer">
        
        <?php 
		
        the_title( '<h2 class="entry-title">', '</h2>' ); 

        $page_id = get_queried_object_id();
        $parent_id = wp_get_post_parent_id($page_id);
        $parent_title = get_the_title($parent_id);
        $parent_title = strtolower($parent_title);
        $parent_title = str_replace(" ", "-", $parent_title);
        wp_nav_menu( array( 'theme_location' => $parent_title, 
                            'container_class' => 'entry-nav',
                          ) );

        /* wp_nav_menu(
            array(
                'menu'            => $parent_title,
                'theme_location'  => 'primary',
                'container_class' => 'collapse navbar-collapse',
                'container_id'    => 'navbarNavDropdown',
                'menu_class'      => 'navbar-nav sub-nav',
                'fallback_cb'     => '',
                'menu_id'         => 'main-menu'
                // 'walker'          => new WP_Bootstrap_Navwalker(),
            )
        ); */ ?>

		<?php edit_post_link( __( 'Edit', 'understrap' ), '<span class="edit-link">', '</span>' ); ?>
        
        <div class="arrow-wrapper-sub">
            <div class="arrow arrow-3-shaft"></div>
            <div class="arrow arrow-3"></div>
            <div class="arrow arrow-3-head"></div>
        </div>
        
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->

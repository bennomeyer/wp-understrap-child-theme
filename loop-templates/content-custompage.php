<?php
/**
 * Partial template for content in page.php
 *
 * @package understrap
 */

?>
    <article <?php post_class(); ?> id="post-
        <?php the_ID(); ?>">

        <header class="entry-header">
            <div class="arrow-wrapper-sub">
                <div class="arrow arrow-1"></div>
                <div class="arrow arrow-2 hidden-sm-down"></div>
            </div>
            
            <!-- Social Media & Language Switcher -->
            <div class="sm-icons-wrapper-sub">
                <!-- Social Media -->
                <!-- <div class="fb"><a target="_blank" href="https://www.facebook.com/blumenbinderzurich/"><img src="https://blumenbinder.ch/wp-content/uploads/2019/06/facebook-48.png"></a></div>
                <div class="insta"><a target="_blank" href="https://www.instagram.com/blumenbinderzh/"><img src="https://blumenbinder.ch/wp-content/uploads/2019/06/instagram-48.png"></a></div>
                <!-- Language Switcher -->
                <div class="language-switcher">
                    <?php
                    if ( is_user_logged_in() ) {
                        custom_language_switcher();
                    } ?>
                </div>

            </div>
            
            <?php 
        // the_title( '<h2 class="entry-title">', '</h2>' ); 

        $page_id = get_queried_object_id();
        $parent_id = wp_get_post_parent_id($page_id);
        $parent_title = get_the_title($parent_id);
        
        $parent_title2 = strtolower($parent_title);
        $parent_title2 = str_replace(" ", "-", $parent_title2);
        $parent_title = str_replace(" ", "<br>", $parent_title);


        /* For english menues */
        if (ICL_LANGUAGE_CODE == "en") {
            $parent_id_de = apply_filters( 'wpml_object_id', $parent_id, 'page', false, 'de' );
            $parent_title_de = get_the_title($parent_id_de);
            $parent_title2 = strtolower($parent_title_de);
            $parent_title2 = str_replace(" ", "-", $parent_title2);
            
        }    
            
            
        echo '<h2 class="entry-title hidden-md-up">'.$parent_title.'</h2>';
        ?>
            <div class="menu-overlay-wrapper">
                <div class="main-menu-bars">
                    <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
                </div>
            </div>
            <div class="menu-overlay menu-overlay-sub menu-visible hidden">
                <?php // wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
                <div class="menu-hauptmenue-container hidden-sm-down">
                    <ul id="menu-hauptmenue" class="menu">
                       <li id="menu-item-2396" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-20 current_page_item menu-item-2396"><a href="https://blumenbinder.ch/"><i class="fa fa-2x fa-home" aria-hidden="true"></i></a>
                                            </li>
                        <li id="menu-item-69" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>#das-angebot">Das Angebot</a>
                        </li>
                        <li id="menu-item-1315" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1315">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>#die-galerie">Die Galerie</a>
                        </li>
                        <!--<li id="menu-item-68" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-68">
                        <a href="<?php // echo esc_url( home_url( '/' ) ); ?>#der-e-shop">Der E-Shop</a>
                        </li>-->
                        <li id="menu-item-67" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-67">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>#die-kunst">Die Kunst</a>
                        </li>
                        <li id="menu-item-66" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>#der-laden">Der Laden</a>
                        </li>
                    </ul>
                </div>
                <!-- mobile menu template part -->
                <?php get_template_part('partials/mobile', 'menu'); ?>
            </div>

            <?php // the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        </header>
        <!-- .entry-header -->

        <div class="entry-content-wrapper">

            <div class="content-wrapper-wrapper">

                <?php /* if (is_page('news')) : */ ?>
                <!-- is news -->
                <?php /* get_template_part('partials/news'); */ ?>

                <?php if (is_page('straeusse')) : ?>
                <!-- is products page -->

                <?php get_template_part('partials/products'); ?>

                <?php else : ?>
                <!-- regular page -->

                <!-- Don't display regular content and loop through all the repeater fields from the page. -->

                <?php /* <div class="content-wrapper row">
                    <div class="entry-content col-md-6">
                        <?php 
                    if ( !empty( get_the_content() ) ) {
                        the_content();
                    } ?>
           	 		</div>
            		<div class="entry-content fit-img fit-img-1st-child col-md-6">
                	<?php 
                    	if ( get_the_post_thumbnail() ) {
                        	echo get_the_post_thumbnail();
                    	} ?>
            		</div>
        		</div> */ ?>


        <?php

            // check if the repeater field has rows of data
            if( have_rows('repeater') ):
            $i = 1;
            // loop through the rows of data
            while ( have_rows('repeater') ) : the_row();

                // display a sub field value
                echo '<div class="content-wrapper row row-'.$i.'">';
                echo '<div class="entry-content content-left left-'.$i.' col-md-6">';
                the_sub_field('content-left');
                echo '</div>';
                echo '<div class="entry-content content-right right-'.$i.' col-md-6">';
                the_sub_field('content-right');
                $i++;

                    //$image = get_sub_field('content-right'); ?>
            <!-- <img src="<?php //echo $image['url']; ?>" alt="<?php //echo $image['alt']; ?>" class="wp-post-image" />-->

            <?php

                echo '</div></div>';

            endwhile;

            else :
            // no rows found
            endif;

            ?>

                <script>
                    jQuery(document).ready(function($) {
                        $('p img').parent().css('margin-bottom', '0px');
                        $('.content-left img').parent().parent().addClass('fit-img-left');
                        $('.content-right img').parent().parent().addClass('fit-img-right');
                        $('.row-1 .left-1 img').first().parent().parent().addClass('fit-img-1st-child');
                        $('.row-1 .content-right img').first().parent().parent().addClass('fit-img-1st-child');
                        $('.entry-content:not(:has(img))').addClass('p-fix');

                        if ($(window).width() > 768) {
                            if ($('.content-left img').length ) {
                                $('.content-left img').css('margin-left', - Math.floor($('.content-left p img').parent().offset().left));
                            }
                        }

                        $(window).resize(function() {
                            if ($(window).width() > 768) {
                                if ($('.content-left img').length) {
                                    $('.content-left img').css('margin-left', - Math.floor($('.content-left p img').parent().offset().left));
                                    console.log(Math.floor($('.content-left p').offset().left));
                                }
                            }
                        });
                    });

                </script>


                <?php endif ?>
                <!-- end not news -->

                </div>


                <?php
                    wp_link_pages( array(
                        'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
                        'after'  => '</div>',
                    ) );
                ?>

                    </div>
                    <!-- .entry-content -->

                    <footer class="entry-footer hidden-sm-down">


                        <?php 

                        echo '<h2 class="entry-title">'.$parent_title.'</h2>';
                        wp_nav_menu( array( 'theme_location' => $parent_title2, 
                            'container_class' => 'entry-nav hidden-sm-down',
                        ) );


        /* wp_nav_menu(
            array(
                'menu'            => $parent_title,
                'theme_location'  => 'primary',
                'container_class' => 'collapse navbar-collapse',
                'container_id'    => 'navbarNavDropdown',
                'menu_class'      => 'navbar-nav sub-nav',
                'fallback_cb'     => '',
                'menu_id'         => 'main-menu'
                // 'walker'          => new WP_Bootstrap_Navwalker(),
            )
        ); */ ?>

                        <?php edit_post_link( __( 'Edit', 'understrap' ), '<span class="edit-link">', '</span>' ); ?>

                        <div class="arrow-wrapper-sub">
                            <!-- <div class="arrow arrow-3-shaft"></div> -->
                            <div class="arrow arrow-3 hidden-sm-down"></div>
                            <!-- <div class="arrow arrow-3-head"></div> -->
                        </div>

                    </footer>
                    <!-- .entry-footer -->

    </article>
    <!-- #post-## -->
